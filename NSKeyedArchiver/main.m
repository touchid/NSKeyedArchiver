//
//  main.m
//  NSKeyedArchiver
//
//  Created by admin on 2017/11/19.
//  Copyright © 2017年 LC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
