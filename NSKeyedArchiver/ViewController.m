//
//  ViewController.m
//  NSKeyedArchiver
//
//  Created by admin on 2017/11/19.
//  Copyright © 2017年 LC. All rights reserved.
//

#import "ViewController.h"
#import "LCSongsMessStatus.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

#pragma mark
#pragma mark - 写入数据
- (IBAction)saveData:(id)sender {
    
    // 实例化对象
LCSongsMessStatus *songsMessStatus = [[LCSongsMessStatus alloc] init];
    
    songsMessStatus.title = @"歌曲一";
    // 路径
    NSString *filePath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES).lastObject stringByAppendingPathComponent:@"songs.txt"];
    
    
    // 归档
    
    /**
     第一个参数: 要保存的对象
     第二个参数: 路径
     */
    [NSKeyedArchiver archiveRootObject:songsMessStatus toFile:filePath];
}
    
    
#pragma mark
#pragma mark - 读取数据
- (IBAction)readData:(id)sender {
    // 路径
    NSString *filePath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES).lastObject stringByAppendingPathComponent:@"songs.txt"];
    
    // 解档 , 归档的时候使用的是什么类型的数据, 解档的时候就使用什么类型
    LCSongsMessStatus *songsMessStatus = [NSKeyedUnarchiver unarchiveObjectWithFile:filePath];
    
    NSLog(@"%@", songsMessStatus.title);
    
    
}


@end
