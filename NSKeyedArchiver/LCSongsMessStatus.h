//
//  LCSongsMessStatus.h
//  NSKeyedArchiver
//
//  Created by admin on 2017/11/19.
//  Copyright © 2017年 LC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LCSongsMessStatus : NSObject
/** 排序号 */
@property (nonatomic, strong)NSString * n;
/** 歌曲ID */
@property (nonatomic, strong)NSString * id;
/** 歌曲名字 */
@property (nonatomic, strong)NSString * title;
/** 歌曲URL */
@property (nonatomic, strong)NSString * fileUrl;
/** 歌曲沙盒路径Path */
@property (nonatomic, strong)NSString * filePath;
/** 歌曲时间 */
@property (nonatomic, strong)NSString * duration;
/** 歌词 */
@property (nonatomic, strong)NSString * TLrcWord;

@property (nonatomic, strong)NSString * mydate;

/** 父ID */
@property (nonatomic, strong)NSString * parentId;
/** 图片 */
@property (nonatomic, strong)NSString * image;
@property (nonatomic, strong)NSString * image300;

/** 专辑名字 */
@property (nonatomic, strong)NSString * album;
/** 专辑名字 */
@property (nonatomic, strong)NSString * singerName;
/** 灵修内容 */
@property (nonatomic, strong)NSString * content;
    

@end
