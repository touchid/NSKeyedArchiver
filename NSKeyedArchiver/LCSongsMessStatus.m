//
//  LCSongsMessStatus.m
//  NSKeyedArchiver
//
//  Created by admin on 2017/11/19.
//  Copyright © 2017年 LC. All rights reserved.
//

#import "LCSongsMessStatus.h"

@implementation LCSongsMessStatus
//归档
/**
    归档

    告诉归档器, 要对对象的哪些属性进行归档
*/
- (void)encodeWithCoder:(NSCoder *)aCoder
{
    //将实例变量转为字节流
    [aCoder encodeObject:self.n forKey:@""];
    [aCoder encodeObject:self.id forKey:@"id"];
    [aCoder encodeObject:self.title forKey:@"title"];
    [aCoder encodeObject:self.fileUrl forKey:@"fileUrl"];
    [aCoder encodeObject:self.filePath forKey:@"filePath"];
    [aCoder encodeObject:self.duration forKey:@"duration"];

    [aCoder encodeObject:self.TLrcWord forKey:@"TLrcWord"];
    [aCoder encodeObject:self.mydate forKey:@"mydate"];
    [aCoder encodeObject:self.parentId forKey:@"parentId"];
    [aCoder encodeObject:self.image forKey:@"image"];
    [aCoder encodeObject:self.image300 forKey:@"image300"];
    [aCoder encodeObject:self.album forKey:@"album"];
    [aCoder encodeObject:self.singerName forKey:@"singerName"];
    [aCoder encodeObject:self.content forKey:@"content"];

    //将数值转为字节流
    //    [aCoder encodeInteger:<#(NSInteger)#> forKey:<#(NSString *)#>];
//    [aCoder encodeInt:self.n forKey:@"n"];
}

//解档
- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    if (self) {
        //用key取二进制对象
        // 只是实例化出来一个对象, 但是对象中属性并没有值, 这些值, 在aDecoder : 接档器
//        self.n = [aDecoder decodeIntForKey:@"n"];
        self.n = [aDecoder decodeObjectForKey:@"n"];
        self.id = [aDecoder decodeObjectForKey:@"id"];
        self.title = [aDecoder decodeObjectForKey:@"title"];
        self.fileUrl = [aDecoder decodeObjectForKey:@"fileUrl"];
        self.filePath = [aDecoder decodeObjectForKey:@"filePath"];
        self.duration = [aDecoder decodeObjectForKey:@"duration"];
        self.TLrcWord = [aDecoder decodeObjectForKey:@"TLrcWord"];
        self.mydate = [aDecoder decodeObjectForKey:@"mydate"];
        self.parentId = [aDecoder decodeObjectForKey:@"parentId"];
        self.image = [aDecoder decodeObjectForKey:@"image"];
        self.image300 = [aDecoder decodeObjectForKey:@"image300"];
        self.album = [aDecoder decodeObjectForKey:@"album"];
        self.singerName = [aDecoder decodeObjectForKey:@"singerName"];
        self.content = [aDecoder decodeObjectForKey:@"content"];
    }
    return self;
}
    

@end
